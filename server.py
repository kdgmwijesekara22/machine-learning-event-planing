from flask import Flask, jsonify
from flask_cors import CORS
from predictions import train_and_predict

app = Flask(__name__)
CORS(app)

@app.route('/predict', methods=['GET'])
def predict():
    # Call the external training function and get the result
    trained_model, predictions = train_and_predict()

    # Convert the predictions to a suitable format for JSON response
    response = predictions.to_json(orient='records')
    return jsonify(response)

if __name__ == '__main__':
    app.run(debug=True)
